from django import forms

from .models import Candidate, PadawanTest


class CandidateForm(forms.ModelForm):
    class Meta:
        model = Candidate
        fields = ('name', 'planet', 'email', 'age')


class PadawanTestForm(forms.ModelForm):
    class Meta:
        model = PadawanTest
        fields = ('question1', 'question2', 'question3','candidate')

    def get_initial(self, candidate):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super(PadawanTestForm, self).get_initial()

        initial['candidate'] = candidate
        return initial
