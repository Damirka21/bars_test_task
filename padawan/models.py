import uuid

from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Candidate(models.Model):
    name = models.CharField(max_length=32)
    planet = models.ForeignKey(Planet, related_name='candidates', blank=True, null=True, on_delete=models.SET_NULL)
    email = models.EmailField()
    age = models.IntegerField()
    jedi = models.ForeignKey('Jedi', blank=True, null=True, related_name='candidates', on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class Jedi(models.Model):
    name = models.CharField(max_length=32)
    planet = models.ForeignKey(Planet, related_name='jedi', blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class PadawanTest(models.Model):
    unique_id = models.UUIDField(default=uuid.uuid1)
    question1 = models.BooleanField(verbose_name="Darth Vader is good?")
    question2 = models.BooleanField(verbose_name="Is Yoda philosopher?")
    question3 = models.BooleanField(verbose_name="You are on right way?")
    candidate = models.ForeignKey(Candidate, blank=True, null=True, related_name="padawan_test",
                                  on_delete=models.SET_NULL)

    def __str__(self):
        return self.candidate.name
