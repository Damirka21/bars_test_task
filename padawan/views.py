from django import forms
from django.core.mail import send_mail
from django.http import HttpResponse

from padawan.models import Candidate, Planet, Jedi, PadawanTest
from .forms import CandidateForm, PadawanTestForm
from django.shortcuts import render
from django.shortcuts import redirect
from django.template import RequestContext


def index(request):
    return render(request, 'padawan/index.html')


def new_candidate(request):
    if request.method == "POST":
        form = CandidateForm(request.POST)
        if form.is_valid():
            candidate = form.save()
            new_form = PadawanTestForm(initial={'candidate': candidate})
            new_form.fields['candidate'].widget = forms.HiddenInput()
            return render(request, 'padawan/new_padawan_test.html', {'form': new_form})
    else:
        form = CandidateForm()
        return render(request, 'padawan/new_candidate.html', {'form': form})


def new_padawan_test(request):
    if request.method == "POST":
        form = PadawanTestForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse("You answers saved! Waiting for Jedi!")
        return HttpResponse("Ops!")


def jedi_choice(request):
    if request.method == "POST":
        jedi_id = request.POST['jedi']
        jedi = Jedi.objects.get(id=jedi_id)
        if len(jedi.candidates.all()) == 3:
            return HttpResponse("You can't have more than 3 Padawans")
        planet = jedi.planet
        candidates = Candidate.objects.filter(planet=planet, jedi=None).all()
        tests = []
        for candidate in candidates:
            tests.append(candidate.padawan_test.get())
        return render(request, 'padawan/candidates.html', {'tests': tests, 'jedi_id': jedi_id})

    else:
        choices = Jedi.objects.all()
        return render(request, 'padawan/jedi_choice.html', {'choices': choices})


def padawan_test(request, num, jedi_id):
    if request.method == "POST":
        test = PadawanTest.objects.get(id=num)
        candidate = test.candidate
        candidate.jedi = Jedi.objects.get(id=jedi_id)
        candidate.save()
        # send email
        send_mail(
            'Congrats!',
            'You are now Padawan!',
            'damir.garifullin96@gmail.com',
            [test.candidate.email],
            fail_silently=False,
        )
        return HttpResponse("You have register Padawan!")
    else:
        test = PadawanTest.objects.get(id=num)
        return render(request, 'padawan/padawan_test.html', {'test': test})


def jedi(request):
    jedi = Jedi.objects.all()
    result = []
    for j in jedi:
        result.append({'name': j.name, 'pad': len(j.candidates.all())})
    return render(request, 'padawan/jedi.html', {'result': result})


def jedi_one(request):
    jedi = Jedi.objects.all()
    result = []
    for j in jedi:
        if len(j.candidates.all()) >= 1:
            result.append({'name': j.name, 'pad': len(j.candidates.all())})
    return render(request, 'padawan/jedi.html', {'result': result})
