from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^new_candidate$', views.new_candidate, name='new_candidate'),
    url(r'^new_padawan_test$', views.new_padawan_test, name='new_padawan_test'),
    url(r'^jedi_choice$', views.jedi_choice, name='jedi_choice'),
    url(r'^padawan_test/(?P<num>[0-9]+)/(?P<jedi_id>[0-9]+)$', views.padawan_test, name='padawan_test'),
    url(r'^jedi$', views.jedi, name='jedi'),
    url(r'^jedi_one$', views.jedi_one, name='jedi_one'),


]