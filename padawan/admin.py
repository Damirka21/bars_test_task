from django.contrib import admin
from .models import Jedi, Candidate, PadawanTest, Planet


# Register your models here.

class JediAdmin(admin.ModelAdmin):
    pass


class CandidateAdmin(admin.ModelAdmin):
    pass


class PadawanTestAdmin(admin.ModelAdmin):
    fields = ('question1', 'question2', 'question3', 'candidate')


class PlanetAdmin(admin.ModelAdmin):
    pass


admin.site.register(Jedi, JediAdmin)
admin.site.register(Candidate, CandidateAdmin)
admin.site.register(PadawanTest, PadawanTestAdmin)
admin.site.register(Planet, PlanetAdmin)
